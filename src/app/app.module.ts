import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './views/traits/navbar/navbar.component';
import { FooterComponent } from './views/traits/footer/footer.component';
import { HomeComponent } from './views/pages/home/home.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { VoteComponent } from './views/pages/vote/vote.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    VoteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
